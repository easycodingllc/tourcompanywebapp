# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version 0.01
* This is an outsourcing web-app project for a tour company.

### How do I get set up? ###

* The project was set up on Aliyun ECS.
* Base on Apache, PHP5.
* Dependencies: Wordpress
* Using MySQL database.
* How to run tests
    > see the private documents.
* Deployment instructions
    > see the private documents.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact